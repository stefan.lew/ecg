# EKG Basiskurs
## Sick-Sinus-Syndrom
---
## Sick-Sinus-Syndrom
### auch Sinusknotensyndrom

beschreibt eine ganze Gruppe von Störungen des Sinusknoten

Sinusknotenstillstand, inadäquate Sinusbradykardie, SA-Block, Tachykardie-Bradykardie-Syndrom, …
---
## Sinusbradykardie
generell als Sinusrhythmus mit Frequenz < 60/min (oft auch < 50/min) definiert

bei jungen, trainierten Erwachsenen, insbesondere AthletInnen oder im Schlaf auch im Bereich bis 30/min nicht unüblich
---
### Beispiel
Sinusbradykardie mit 40/min bis 45/min

![](/assets/img/ecgs/sinus_bradycardia.png)
---
### Beispiel
extreme Sinusbradykardie mit Ersatzschlägen (2,4) und "*fusion beat*" (3)

![](/assets/img/ecgs/sinus_bradycardia_escape_fusion.png)
---
### Krankheitswert
Sinusbradykardie gilt als Normalbefund während des Schlafs, bei trainierten PatientInnen, während einer vasovagalen Synkope und bei Vagusmanövern
---
### Krankheitswert
in allen anderen Fällen gilt Sinusbradykardie als pathologisch und sollte an Herzinfarkt, Medikamentenwirkungen, erhöhten Hirndruck, Hypothermie, Hyperkaliämie, … denken lassen
---
## Sinusknotenstillstand
### je nach Dauer auch Sinuspause oder Sinusarrest
verursacht durch fehlende Erregungsbildung im Sinuskonten oder fehlende Überleitung an den Vorhof (SA-Block)
---
am EKG fehlen P-Wellen

es können Ersatzrhythmen auftreten
---
### Beispiel
Sinuspause von 3 Sekunden

![](/assets/img/ecgs/sinus_pause.png)
---
### Beispiel
Sinuspause von 3 Sekunden

![](/assets/img/ecgs/sinus_pause2.png)
---
### Beispiel
SA-Block 2:1

![](/assets/img/ecgs/sa_block_2-1.png)
---
### Beispiel
Sinusarrest mit Ersatzrhyhtmus aus dem AV-Knoten

![](/assets/img/ecgs/sinus_pause_escape.png)
---
### Symptome
PatientInnen können über Schwäche, Schwindel, Verwirrtheit, Synkopen, Angina Pectoris und Atemnot klagen
---
## Tachykardie-Bradykardie-Syndrom
Sinusknotenstörungen gehen mit einem hohen Riskio für supraventrikuläre Tachykardien einher

insbesondere Vorhofflimmern und Vorhofflattern
---
### Beispiel
![](/assets/img/ecgs/tachy_brady.png)
---
### Beispiel
![](/assets/img/ecgs/tachy_brady2.jpg)
---
## Übungsbeispiele
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case455/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case221/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case510/1350x900.gif)