# EKG Basiskurs
## Sinusarrhythmie
---
## Sinusarrhythmie
### auch Respiratorische Sinusarrhythmie

erfüllt alle Kriterien von normalem Sinusrhythmus,

allerdings schwankt die Herzfrequenz mit Aus- und Einatmung
---
### Atemabhängigkeit
bei der Einatmung steigt die Frequenz des Sinusknotens, bei der Ausatmung sinkt die Frequenz

bei Luftanhalten bleibt die Frequenz gleich
---
### Beispiel
Sinusarrhythmie mit 50/min bis 75/min

![Sinusarrhythmie](/assets/img/ecgs/sinus_arrhythmia.png)
---
betroffen sind meist Kinder, Jugendliche und eher jüngere und/oder trainierte Erwachsene 
---
### Krankheitswert
Respiratorische Sinusarrhythmie gilt als Normalbefund bei jüngeren PatientInnen

insbesondere wenn die Arrhythmie eindeutig atemabhängig ist
---
### Krankheitswert
bleibt die Arryhthmie bei Luftanhalten bestehen, zeigt die niedrige Herzfrequenz Symptome oder tritt sie bei älteren, insbesondere nichttrainierten PatientInnen auf, sollte eine Abklärung erfolgen
---
## Übungsbeispiele
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case217/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case245/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case269/1350x900.gif)