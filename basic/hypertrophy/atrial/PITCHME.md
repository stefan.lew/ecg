# EKG Basiskurs
## Atriale Abnormitäten
---
## Atriale Abnormitäten
### früher auch P pulmonale und P mitrale
EKG-Veränderungen durch Hypertrophie, Dilatation und andere strukturelle Veränderungen der Vorhöfe werden heute unter dem Begriff "atriale Abnormitäten" zusammengefasst.
---
### Ursachen
Atriale Abnormitäten stellen Druck- oder Volumenüberlastung der Vorhöfe (zB durch Klappendefekte) dar.

Insbesondere bei Einsetzen einer Dilatation der Vorhöfe kann sich die elektrische Aktivität am EKG verändern.
---
### P-Welle
Die P-Welle entsteht durch eine Überlagerung der Aktivität von rechtem und linkem Vorhof.
---
### Rechtsatriale Abnormität
Der rechstatriale Anteil an der P-Welle nimmt zu.

Dadurch steigt die Amplitude der P-Welle, sie wird aber nicht nennenswert breiter.

![](/assets/img/ecgs/raa.png)
---
### Rechtsatriale Abnormität
Die P-Wellen in II, aVF und III werden höher - über 2,5 mm.

In den Ableitungen V1-2 kann die P-Welle unverändert bleiben, allerdings auch deutlich positiv, oder initial positiv werden.
---
### Rechtsatriale Abnormität
Ein qR in V1 ohne vorangegangenen Infarkt korreliert positiv mit rechtsatrialer Vergrößerung.

Auch eine abrupte Größenzunahme der R-Zacke von V1 auf V2 weist auf eine Vergrößerung des rechten Vorhofs hin.
---
### Linksatriale Abnormität
Durch die spätere Erregung des linken Atriums verstärkt sich der letzte Anteil der P-Welle.

Die P-Welle ist breiter als normal - mehr als 110ms bei Erwachsenen.
---
### Linksatriale Abnormität
Es können sich sogenannte doppelgipflige P-Wellen zeigen.

In V1 kann der zweite (dort negative) Anteil besonders betont sein.
---
### Linksatriale Abnormität
![](/assets/img/ecgs/laa.png)

P-Wellen bei LAA.
---
### Linksatriale Abnormität
Die Achse der P-Welle ist nach links verschoben und liegt meist bei 0° bis -30°.
---
### Biatriale Abnormität
Es können natürlich auch beide Vorhöfe betroffen sein.

Dabei zeigen sich Kombinationen der vorherigen Hinweise.
---
### Zusammenfassung
![](/assets/img/ecgs/atriale-abnorm.png)
---
### Übungsbeispiele
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case225/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case381/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case153/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case382/1350x900.gif)