# EKG Basiskurs
## Perikarditis
---
## Perikarditis
### auch Herzbeutelentzündung

Akute Perikarditis geht fast immer mit einer Entzündung der direkt unterhalb des Perikards liegenden Schichten des Herzmuskelgewebes einher (Perimyokarditis).
---
### EKG-Veränderungen
Innerhalb weniger Stunden nach Symptombeginn kann man ST-Strecken-Hebungen erkennen.

Diese zeigen sich diffus und nicht wie bei Infarkten auf gewisse Areale begrenzt.
---
### EKG-Veränderungen
In Ableitungen mit negativen T-Wellen (aVR, V1) können sich konkordante Senkungen zeigen.

Abgesehen davon treten keine, für akute Verschlüsse typische, spiegelbildlichen Senkungen auf.
---
### EKG-Veränderungen
Sofern keine anderen Ursachen, wie vergangene Infarkte vorliegen, zeigen sich auch keine Q-Zacken. 
---
### Spodick's Sign
In fast zwei Drittel der Fälle ist die PQ-Strecke gesenkt.

Diese Besonderheit bezeichnet man als "Spodick's Sign".

![](https://litfl.com/wp-content/uploads/2018/08/V5-pericarditis.jpg)
---
### Beispiel
![](https://litfl.com/wp-content/uploads/2018/08/ECG-Pericarditis-3.jpg)
---
### Sinustachykardie
Sinustachykardien sind häufig.

Sie können durch einen Infekt mit Fieber aber auch durch hämodynamische Probleme bei Perikarderguss auftreten.
---
### Perikarderguss
Ausgelöst durch die Perikarditis kann es als Komplikation zu Perikarderguss bzw -tamponade kommen.

Dies zeigt sich am EKG womöglich durch niedrige Spannung.
---
### Beispiel
![](/assets/img/ecgs/acute_pericarditis_tamponade.png)
---
### Abgrenzung zu ACS
Die Abgrenzung zu ACS ist oft schwierig, da die EKG-Befunde wie ST-Strecken-Hebungen für beide Ursachen sprechen können.

Hinzu kommt, dass beide Erkrankungen präklinisch anhand Brustschmerzen schwer unterschieden werden können.
---
### Übungsbeispiele
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case363/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case464/1350x900.gif)
---
![](https://ecg.bidmc.harvard.edu/mavendata/images/case3/1350x900.gif)